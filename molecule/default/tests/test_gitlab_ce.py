import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-gitlab-ce')


def test_gitlab_ce_installed(host):
    assert host.package('gitlab-ce').is_installed
