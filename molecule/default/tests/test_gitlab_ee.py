import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("gitlab-ee.esss.lu.se")


def test_gitlab_ee_installed(host):
    package = host.package("gitlab-ee")
    assert package.is_installed
    assert package.version == "13.2.9"
