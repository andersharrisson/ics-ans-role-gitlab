# ics-ans-role-gitlab

Ansible role to install GitLab.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
# Set to ce (community edition) or ee (enterprise edition)
gitlab_edition: ce
# Set to latest or the exact version number
gitlab_version: "latest"
gitlab_external_url: "http://{{ ansible_fqdn }}"
gitlab_email_from: noreply@esss.se
gitlab_email_display_name: "ESS GitLab"
gitlab_email_reply_to: noreply@esss.se
gitlab_default_theme: 6
gitlab_default_projects_features_issues: "true"
gitlab_lfs_enabled: "true"
gitlab_lfs_storage_path: "/var/opt/gitlab/gitlab-rails/shared/lfs-objects"
gitlab_ldap_enabled: "false"
gitlab_ldap_host: "_your_ldap_server"
gitlab_ldap_port: 389
gitlab_ldap_bind_dn: "_the_full_dn_of_the_user_you_will_bind_with"
gitlab_ldap_password: secret
gitlab_ldap_base: ""
gitlab_ldap_encryption: start_tls
gitlab_ldap_user_filter: ""
gitlab_ldap_group_base: ""
gitlab_ldap_admin_group: ""
gitlab_backup_path: /var/opt/gitlab/backups
gitlab_backup_keep_time: 604800
gitlab_nginx_redirect_http_to_https: "false"
gitlab_nginx_ssl_certificate: "{{ certificate_cert_chain_path }}"
gitlab_nginx_ssl_certificate_key: "{{ certificate_key_path }}"
gitlab_pages_external_url: ""
gitlab_pages_access_control: "true"
gitlab_mattermost_enable: "false"
gitlab_mattermost_external_url: ""
gitlab_registry_enabled: "false"
gitlab_registry_external_url: https://registry.gitlab.example.com
gitlab_registry_path: /var/opt/gitlab/gitlab-rails/shared/registry
gitlab_smtp_enable: "false"
gitlab_smtp_address: smtp.server
gitlab_smtp_port: 465
gitlab_smtp_user_name: "smtp user"
gitlab_smtp_password: "smtp password"
gitlab_smtp_domain: "example.com"
gitlab_smtp_authentication: login
gitlab_smtp_enable_starttls_auto: "true"
gitlab_smtp_tls: "true"
gitlab_prometheus_listen_addr_host: "localhost"
gitlab_udp_log_shipping_host: ""
gitlab_udp_log_shipping_port: 514
# WARNING: Minimum required worker_processes is 2
gitlab_puma_worker_processes: 2
gitlab_grafana_enable: true
gitlab_grafana_admin_password: admin
gitlab_validate_certs: true
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-gitlab
```

## License

BSD 2-clause
